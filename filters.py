"""Modul zawiera funkcje filtrujace querysety.

Funkcje te w wiekszosci przypadkow powinny wykorzystane w definicji
nowej funkcje filtrujacej, nie zamiast jej, poniewaz maja nieodpowiednie
sygnatury.

"""


def icontains_filter(data, query, *args):
    """Filtruje tabele.

    Uzywa `icontains` lookup na kazdej nazwie pola z args, robiac na nich `or`.

    Args:
        data (queryset): queryset do przefiltrowania
        query (string)
        args ([string]): nazwy pol ktore maja byc uwzglednione w filtrowaniu

    Returns:
        przefiltrowany queryset
    """
    from django.db.models import Q
    q = Q()
    for field_name in args:
        q |= Q(**{field_name + '__icontains': query})
    try:
        return data.filter(q)
    except AttributeError:
        return data


def make_icontains_filter(*args):
    """Tworzy funkcje filtrujaca.

    Args:
        *args: nazwy pol po ktorych ma filtrowac (uzywajac __icontains)
    """
    def f(self, data, query):
        return icontains_filter(data, query, *args)
    return f

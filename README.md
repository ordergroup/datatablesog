# Datatables 1.0.0.
## Instalacja

Przenies ten folder do swojego projektu django i dodaj `datatables` do `INSTALLED_APPS`. To co jest w static przenies do swoich static w public_django.
Do strony na ktorej chcesz uzyc tabelek dodaj

```
    <script src="{{STATIC_URL}}datatables/datatables.js"></script>
    <link href="{{STATIC_URL}}datatables/datatables.css" rel="stylesheet" type="text/css">
```

## Wymagania

- Dajaxice
- jQuery
- prawdopdobnie boostrap (TODO: sprawdzic)
- mozliwe, ze jakis admin (TODO: sprawdzic)

## Uzywanie
Tak naprawde to przeczytaj docstringi w *table.py* i moze w *filters.py*

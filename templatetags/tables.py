from django import template

register = template.Library()


@register.simple_tag()
def datatable(table):
    return table.render()


@register.filter
def has_selected(key, value):
    return key, value


@register.filter
def in_table(item, table):
    key, value = item
    value = unicode(value)
    print table.dropdown_filters_values
    print key, value
    if table.dropdown_filters_values:
        if unicode(table.dropdown_filters_values.get(key, None)) == value:
            return 'selected="selected"'
    return ''
